﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace mtWebDraw.Common
{

    /// <summary>
    /// 图表授权权限码，对应JS中数组（JS中写死于前台）
    /// </summary>
    public enum ChartCode
    {
        无权限 = 0,
        仅查看 = 11,
        可编辑 = 21,
        可删除 = 41
    }
}